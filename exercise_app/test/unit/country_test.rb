require 'test_helper'

class CountryTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_save_without_code
  	country = Country.new(:name=>'new_name')
  	assert_equal country.valid?, false
  	assert_equal country.save, false
  end
  def test_save_without_name
  	country = Country.new(:code=>'usa')
  	assert_equal country.valid?, false
  	assert_equal country.save, false
  end
  def test_save_with_unallowed_code
  	country = Country.new(:code=>'jpn',:name=>'new_name')
  	assert_equal country.valid?, false
  	assert_equal country.save, false
  end
  def test_save_with_correct_code_and_name
    country = Country.new(:code=>'usa',:name=>'new_name')
    assert_equal country.valid?, true
    assert_equal country.save, true
  end
  # scope indo, has many indonesian
  def test_find_indonesian_by_scope
    country = Country.create([
      {:code=>'usa',:name=>'united states'}, {:code=>'id',:name=>'indonesia'},
    ])
    indonesia = Country.where(:name=>"indonesia")
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>indonesia[0].id)
    user.encrypt_password
    user.save
    assert_not_nil User.indo
    assert_equal User.indo[0].country_id, indonesia[0].id
  end
  #def test_find_indonesian_by_override_relation
  #  indonesia = Country.where(:name=>"indonesia")
  #  user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
  #    :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>indonesia[0].id)
  #  user.encrypt_password
  #  user.save
  #  country = Country.find(indonesia[0].id)
  #  assert_not_nil country.indonesian
  #  assert_equal country.indonesian[0].country_id, indonesia[0].id
  #end
  
  def test_relation_between_country_and_user
    country = Country.create(:code=>'usa',:name=>'new_name')
    assert_not_nil country
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>country.id)
    user.encrypt_password
    user.save
    assert_not_nil country.users
    assert_equal country.users.empty?, false
    assert_equal country.users[0].class, User
  end
end
