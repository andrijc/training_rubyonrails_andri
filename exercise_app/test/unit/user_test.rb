require 'test_helper'

class UserTest < ActiveSupport::TestCase
  # test "the truth" do
  #   assert true
  # end
  def test_save_without_first_name
  	user = User.new(:last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_last_name
  	user = User.new(:first_name => 'first',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_email
  	user = User.new(:first_name => 'first', :last_name=>'last',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_username
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_password
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_date_of_birth
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_age
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_address
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:country_id=>'2')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_without_country_id
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address')
    user.encrypt_password
  	assert_equal user.valid?, false
  	assert_equal user.save, false
  end
  def test_save_with_first_name_last_name_email_username_password_dob_age_address_and_country_id
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
    assert_equal user.valid?, true
    assert_equal user.save, true
  end
  def test_find_indonesian_users
  	#country = Country.create(:code=>'id',:name=>'indonesia')
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>countries(:id).id)
    user.encrypt_password
    user.save
    assert_not_nil User.indo
    assert_not_nil User.indo[0]
    assert_equal User.indo[0].country_id, user.country_id
  end
  def test_find_my_country_maker
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
    user.save
    article = Article.create(:title => "my country", :description => "new content",:rating=>"30",:user_id=>user.id)
    u =User.find(user.id)
    assert_not_nil u.my_country
    assert_equal u.my_country[0].title, "my country"
  end
  def test_show_full_address
  	country = Country.create(:code=>'usa',:name=>'new_name')
    assert_not_nil country
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>country.id)
    user.encrypt_password
    user.save
    assert_not_nil user.show_full_address
  end
  def test_show_users_age_more_than_18
  	user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'3')
    user.encrypt_password
    user.save
    assert_not_nil user
    assert_not_nil User.age_18_more
    assert_equal User.age_18_more[0].age, 25
  end
  def test_relation_between_user_and_product
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
    user.save
    assert_not_nil user
    product = Product.create(:name => 'new_prod', :price => "20000",:stock=>32,:description=>"new_desc",:user_id=>user.id)
    assert_not_nil product
    assert_not_nil user.products
    assert_equal user.products.empty?, false
    assert_equal user.products[0].class, Product
  end
  def test_relation_between_user_and_article
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2')
    user.encrypt_password
    user.save
    assert_not_nil user
    article = Article.create(:title => "new_title", :description => "new content",:rating=>"30",:user_id=>user.id)
    assert_not_nil article
    assert_not_nil user.articles
    assert_equal user.articles.empty?, false
    assert_equal user.articles[0].class, Article
  end

  def test_relation_between_user_and_comment
    user = User.new(:first_name => 'first', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'4')
    user.encrypt_password
    user.save
    assert_not_nil user
    comment = Comment.create(:content=>'new_name',:article_id=>'1',:user_id=> user.id)
    assert_not_nil comment
    assert_not_nil user.comments
    assert_equal user.comments.empty?, false
    assert_equal user.comments[0].class, Comment
  end
end
