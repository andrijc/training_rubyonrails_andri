require 'test_helper'

class CategoriesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @category = Category.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:categories)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:category)
    assert_response :success
  end

  def test_create
  	login_as('admin@gmail.com')
    assert_difference('Category.count') do
      post :create, :category => {:name=>'new name',:user_id=>'2'}
      assert_not_nil assigns(:category)
            assert_equal assigns(:category).name, "new name"
            assert_equal assigns(:category).valid?, true
    end
    assert_response :redirect
    assert_redirected_to category_path(assigns(:category))
    assert_equal flash[:notice], 'Category was successfully created.'
  end

  def test_create_with_invalid_parameter
  	login_as('admin@gmail.com')
    assert_no_difference('Category.count') do
      post :create, :category => {:name=>nil,:user_id=>nil}
      assert_not_nil assigns(:category)
      assert_equal assigns(:category).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Category was failed to create.'
  end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => Category.first.id
    assert_not_nil assigns(:category)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:category)
    assert_response :redirect
    assert_redirected_to categories_path
        assert_equal flash[:notice], 'Cannot find the category'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => Category.first.id
    assert_not_nil assigns(:category)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:category)
    assert_response :redirect
    assert_redirected_to categories_path
        assert_equal flash[:notice], 'Cannot find the category'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => Category.first.id,
                 :category => {:name=>'updated name',:user_id=>'2'}
    assert_not_nil assigns(:category)
          assert_equal assigns(:category).name, 'updated name'
           assert_response :redirect
    assert_redirected_to category_path(assigns(:category))
        assert_equal flash[:notice], 'Category was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :category => {:name=>'new name',:user_id=>'2'} #rescue nil
    assert_nil assigns(:category)
    assert_response :redirect
    assert_redirected_to categories_path
        assert_equal flash[:notice], 'Cannot find the category'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => Category.first.id,
                 :category => {:name=>nil,:user_id=>nil}
    assert_not_nil assigns(:category)
    assert_response :success
        assert_equal flash[:error], 'Category was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('Category.count', -1) do
      delete :destroy, :id => Category.first.id
      assert_not_nil assigns(:category)
    end
    assert_response :redirect
    assert_redirected_to categories_path
        assert_equal flash[:error], 'Category successfully deleted'
  end
end
