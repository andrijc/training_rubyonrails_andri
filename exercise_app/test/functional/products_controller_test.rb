require 'test_helper'

class ProductsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @product = Product.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:products)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:product)
    assert_response :success
  end

  def test_create
  	login_as('admin@gmail.com')
    assert_difference('Product.count') do
      post :create, :product => {:name=>'new name',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2'}
      assert_not_nil assigns(:product)
            assert_equal assigns(:product).name, "new name"
            assert_equal assigns(:product).valid?, true
    end
    assert_response :redirect
    assert_redirected_to product_path(assigns(:product))
    assert_equal flash[:notice], 'Product was successfully created.'
  end

  def test_create_with_invalid_parameter
  	login_as('admin@gmail.com')
    assert_no_difference('Product.count') do
      post :create, :product => {:name=>nil,:description=>nil,:price=>nil,:stock=>nil,:user_id=>nil}
      assert_not_nil assigns(:product)
      assert_equal assigns(:product).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Product was failed to create.'
  end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => Product.first.id
    assert_not_nil assigns(:product)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:product)
    assert_response :redirect
    assert_redirected_to products_path
        assert_equal flash[:notice], 'Cannot find the product'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => Product.first.id
    assert_not_nil assigns(:product)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:product)
    assert_response :redirect
    assert_redirected_to products_path
        assert_equal flash[:notice], 'Cannot find the product'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => Product.first.id,
                 :product => {:name=>'updated name',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2'}
    assert_not_nil assigns(:product)
          assert_equal assigns(:product).name, 'updated name'
           assert_response :redirect
    assert_redirected_to product_path(assigns(:product))
        assert_equal flash[:notice], 'Product was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :product => {:name=>'updated name',:description=>'new_desc',:price=>'10000',:stock=>'23',:user_id=>'2'} #rescue nil
    assert_nil assigns(:product)
    assert_response :redirect
    assert_redirected_to products_path
        assert_equal flash[:notice], 'Cannot find the product'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => Product.first.id,
                 :product => {:name=>nil,:description=>nil,:price=>nil,:stock=>nil,:user_id=>nil}
    assert_not_nil assigns(:product)
    assert_response :success
        assert_equal flash[:error], 'Product was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('Product.count', -1) do
      delete :destroy, :id => Product.first.id
      assert_not_nil assigns(:product)
    end
    assert_response :redirect
    assert_redirected_to products_path
        assert_equal flash[:error], 'Product successfully deleted'
  end
end
