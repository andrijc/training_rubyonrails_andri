require 'test_helper'

class UsersControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @user  = User.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:users)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:user)
    assert_response :success
  end

  def test_create
  	login_as('admin@gmail.com')
    assert_difference('User.count') do
      post :create, :user => {:first_name => 'new fn', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2'}
      assert_not_nil assigns(:user)
            assert_equal assigns(:user).first_name, "new fn"
            assert_equal assigns(:user).valid?, true
    end
    assert_response :redirect
    assert_redirected_to user_path(assigns(:user))
    assert_equal flash[:notice], 'User was successfully created.'
  end

  def test_create_with_invalid_parameter
  	login_as('admin@gmail.com')
    assert_no_difference('User.count') do
      post :create, :user => {:first_name => nil, :last_name=>nil,:email=>nil,:username=>nil,
      :password=>nil,:date_of_birth=>nil,:age=>nil,:address=>nil,:country_id=>nil}
      assert_not_nil assigns(:user)
      assert_equal assigns(:user).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'User was failed to create.'
  end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => User.first.id
    assert_not_nil assigns(:user)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:user)
    assert_response :redirect
    assert_redirected_to users_path
        assert_equal flash[:notice], 'Cannot find the user'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => User.first.id
    assert_not_nil assigns(:user)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:user)
    assert_response :redirect
    assert_redirected_to users_path
        assert_equal flash[:notice], 'Cannot find the user'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => User.first.id,
                 :user => {:first_name => 'updated fn', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2'}
    assert_not_nil assigns(:user)
          assert_equal assigns(:user).first_name, 'updated fn'
           assert_response :redirect
    assert_redirected_to user_path(assigns(:user))
        assert_equal flash[:notice], 'User was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :user => {:first_name => 'updated fn', :last_name=>'last',:email=>'email@gom.com',:username=>'first_last',
      :password=>'123',:date_of_birth=>'20-09-1983',:age=>'25',:address=>'new_address',:country_id=>'2'} #rescue nil
    assert_nil assigns(:user)
    assert_response :redirect
    assert_redirected_to users_path
        assert_equal flash[:notice], 'Cannot find the user'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => User.first.id,
                 :user => {:first_name => nil, :last_name=>nil,:email=>nil,:username=>nil,
      :password=>nil,:date_of_birth=>nil,:age=>nil,:address=>nil,:country_id=>nil}
    assert_not_nil assigns(:user)
    assert_response :success
        assert_equal flash[:error], 'User was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('User.count', -1) do
      delete :destroy, :id => User.first.id
      assert_not_nil assigns(:user)
    end
    assert_response :redirect
    assert_redirected_to users_path
        assert_equal flash[:error], 'User successfully deleted'
  end
end
