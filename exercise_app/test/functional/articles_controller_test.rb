require 'test_helper'

class ArticlesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end

  def setup
    @article = Article.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:articles)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_create
  	login_as('admin@gmail.com')
    assert_difference('Article.count') do
      post :create, :article => {:title=>'new_title',:description=>'new_desc',:rating=>'1',:user_id=>'2'}
      assert_not_nil assigns(:article)
            assert_equal assigns(:article).title, "new_title"
            assert_equal assigns(:article).valid?, true
    end
    assert_response :redirect
    assert_redirected_to article_path(assigns(:article))
    assert_equal flash[:notice], 'Article was successfully created.'
  end

  def test_create_with_invalid_parameter
  	login_as('admin@gmail.com')
    assert_no_difference('Article.count') do
      post :create, :article => {:title=>nil,:description=>nil,:rating=>nil,:user_id=>nil}
      assert_not_nil assigns(:article)
      assert_equal assigns(:article).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Article was failed to create.'
  end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Cannot find the article'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => Article.first.id
    assert_not_nil assigns(:article)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Cannot find the article'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => Article.first.id,
                 :article => {:title=>'updated title',:description=>'new_desc',:rating=>'1',:user_id=>'2'}
    assert_not_nil assigns(:article)
          assert_equal assigns(:article).title, 'updated title'
           assert_response :redirect
    assert_redirected_to article_path(assigns(:article))
        assert_equal flash[:notice], 'Article was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :article => {:title=>'updated title',:description=>'updated desc',:rating=>'1',:user_id=>'2'} #rescue nil
    assert_nil assigns(:article)
    assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:notice], 'Cannot find the article'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => Article.first.id,
                 :article => {:title=>nil,:description=>nil,:rating=>nil,:user_id=>nil}
    assert_not_nil assigns(:article)
    assert_response :success
        assert_equal flash[:error], 'Article was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('Article.count', -1) do
      delete :destroy, :id => Article.first.id
      assert_not_nil assigns(:article)
    end
    assert_response :redirect
    assert_redirected_to articles_path
        assert_equal flash[:error], 'Article successfully deleted'
  end
end
