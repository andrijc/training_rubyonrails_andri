require 'test_helper'

class CountriesControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @country = Country.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:countries)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:country)
    assert_response :success
  end

  def test_create
  	login_as('admin@gmail.com')
    assert_difference('Country.count') do
      post :create, :country => {:code=>'usa',:name=>'new name'}
      assert_not_nil assigns(:country)
            assert_equal assigns(:country).name, "new name"
            assert_equal assigns(:country).valid?, true
    end
    assert_response :redirect
    assert_redirected_to country_path(assigns(:country))
    assert_equal flash[:notice], 'Country was successfully created.'
  end

  def test_create_with_invalid_parameter
  	login_as('admin@gmail.com')
    assert_no_difference('Country.count') do
      post :create, :country => {:code=>nil,:name=>nil}
      assert_not_nil assigns(:country)
      assert_equal assigns(:country).valid?, false
    end
    assert_response :success
    assert_equal flash[:error], 'Country was failed to create.'
  end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => Country.first.id
    assert_not_nil assigns(:country)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:country)
    assert_response :redirect
    assert_redirected_to countries_path
        assert_equal flash[:notice], 'Cannot find the country'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => Country.first.id
    assert_not_nil assigns(:country)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:country)
    assert_response :redirect
    assert_redirected_to countries_path
        assert_equal flash[:notice], 'Cannot find the country'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => Country.first.id,
                 :country => {:code=>'usa',:name=>'updated name'}
    assert_not_nil assigns(:country)
          assert_equal assigns(:country).name, 'updated name'
           assert_response :redirect
    assert_redirected_to country_path(assigns(:country))
        assert_equal flash[:notice], 'Country was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :country => {:content=>'new content',:article_id=>'2',:user_id=>'2'} #rescue nil
    assert_nil assigns(:country)
    assert_response :redirect
    assert_redirected_to countries_path
        assert_equal flash[:notice], 'Cannot find the country'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => Country.first.id,
                 :country => {:code=>nil,:name=>nil}
    assert_not_nil assigns(:country)
    assert_response :success
        assert_equal flash[:error], 'Country was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('Country.count', -1) do
      delete :destroy, :id => Country.first.id
      assert_not_nil assigns(:country)
    end
    assert_response :redirect
    assert_redirected_to countries_path
        assert_equal flash[:error], 'Country successfully deleted'
  end
end
