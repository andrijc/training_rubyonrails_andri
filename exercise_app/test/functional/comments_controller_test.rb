require 'test_helper'

class CommentsControllerTest < ActionController::TestCase
  # test "the truth" do
  #   assert true
  # end
  def setup
    @comment = Comment.find(:first)
  end

  def test_index
  	login_as('admin@gmail.com')
    get :index
    assert_response :success
    assert_not_nil assigns(:comments)
  end

  def test_new
  	login_as('admin@gmail.com')
    get :new
    assert_not_nil assigns(:comment)
    assert_response :success
  end

  #def test_create
  #	login_as('admin@gmail.com')
  #  assert_difference('Comment.count') do
  #    post :create, :comment => {:content=>'new content',:article_id=>'2',:user_id=>'2'}
  #    assert_not_nil assigns(:comment)
  #          assert_equal assigns(:comment).content, "new content"
  #          assert_equal assigns(:comment).valid?, true
  #  end
  #  assert_response :redirect
  #  assert_redirected_to comment_path(assigns(:comment))
  #  assert_equal flash[:notice], 'Comment was successfully created.'
  #end

  #def test_create_with_invalid_parameter
  #	login_as('admin@gmail.com')
  #  assert_no_difference('Comment.count') do
  #    post :create, :comment => {:content=>nil,:article_id=>nil,:user_id=>nil}
  #    assert_not_nil assigns(:comment)
  #    assert_equal assigns(:comment).valid?, false
  #  end
  #  assert_response :success
  #  assert_equal flash[:error], 'Comment was failed to create.'
  #end

  def test_show
  	login_as('admin@gmail.com')
    get :show, :id => Comment.first.id
    assert_not_nil assigns(:comment)
    assert_response :success
  end

  def test_show_with_undefined_id
  	login_as('admin@gmail.com')
    get :show, :id => Time.now.to_i
    assert_nil assigns(:comment)
    assert_response :redirect
    assert_redirected_to comments_path
        assert_equal flash[:notice], 'Cannot find the comment'
  end

  def test_edit
  	login_as('admin@gmail.com')
    get :edit, :id => Comment.first.id
    assert_not_nil assigns(:comment)
    assert_response :success
  end
  def test_edit_with_undefined_id
  	login_as('admin@gmail.com')
    get :edit, :id => Time.now.to_i
    assert_nil assigns(:comment)
    assert_response :redirect
    assert_redirected_to comments_path
        assert_equal flash[:notice], 'Cannot find the comment'
  end
  def test_update
  	login_as('admin@gmail.com')
    put :update, :id => Comment.first.id,
                 :comment => {:content=>'updated content',:article_id=>'2',:user_id=>'2'}
    assert_not_nil assigns(:comment)
          assert_equal assigns(:comment).content, 'updated content'
           assert_response :redirect
    assert_redirected_to comment_path(assigns(:comment))
        assert_equal flash[:notice], 'Comment was successfully updated.'
  end
  def test_update_with_undefined_id
  	login_as('admin@gmail.com')
    put :update, :id => Time.now.to_i,
                 :comment => {:content=>'new content',:article_id=>'2',:user_id=>'2'} #rescue nil
    assert_nil assigns(:comment)
    assert_response :redirect
    assert_redirected_to comments_path
        assert_equal flash[:notice], 'Cannot find the comment'
  end
  def test_update_with_invalid_parameter
  	login_as('admin@gmail.com')
    put :update, :id => Comment.first.id,
                 :comment => {:content=>nil,:article_id=>nil,:user_id=>nil}
    assert_not_nil assigns(:comment)
    assert_response :success
        assert_equal flash[:error], 'Comment was failed to update.'
  end

  def test_destroy
  	login_as('admin@gmail.com')
    assert_difference('Comment.count', -1) do
      delete :destroy, :id => Comment.first.id
      assert_not_nil assigns(:comment)
    end
    assert_response :redirect
    assert_redirected_to comments_path
        assert_equal flash[:error], 'Comment successfully deleted'
  end
end
