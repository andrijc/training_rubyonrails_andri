class UserMailer < ActionMailer::Base
  default from: "andrijc@gmail.com"
  #:default_from => "eifion@asciicasts.com"
  def registration_confirmation(user)
    @user = user
    mail(:to => user.email, :subject => "Registered")
  end
end
