class CountryController < ApplicationController
  def list
     @country = Country.find(:all)
  end
  def index
    @country = Country.find(:all)
  end
  def new
    @country = Country.new
  end
  def create
    @country = Country.new(params[:country])
    if @country.save
       render :action => 'index'
    else
      render :action => 'new'
    end
    #render :action => :new
    #redirect_to :controller => :users, :action => :index
  end
  def edit
    
  end
  def show
      @country = Country.find(params[:id])
  end
end
