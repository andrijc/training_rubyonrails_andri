class UsersController < ApplicationController
  before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
  before_filter :find_user, :only => [:edit, :update, :show, :destroy]
  


  def index
    @users = User.find(:all)
  end
  def new
    @user = User.new
  end
  def create
    @user = User.new(params[:user])
    if verify_recaptcha
      
      if @user.save 
        UserMailer.registration_confirmation(@user).deliver
        flash[:notice] = "User was successfully created."
        redirect_to (@user)
        #redirect_to root_url, :notice => "Signed up!" 
      else
        flash[:error] = 'User was failed to create.'
        render :action => 'new'
      end
    else
      flash[:error] = "There was an error with the recaptcha code below.
                       Please re-enter the code and click submit."
      render "new"
    end

  end
  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(params[:user])
      flash[:notice] = 'User was successfully updated.'
      redirect_to(@user)
    else
      flash[:error] = 'User was failed to update.'
      render :action => "edit"
    end
  end
  def show
      @user = User.find(params[:id])
  end
  def destroy
    @user = User.find(params[:id])
    @user.destroy ? (flash[:error] = "User successfully deleted") :
                  (flash[:notice] = "User failed to delete")
    redirect_to users_url
  end
  private
    def find_user
      @user = User.find_by_id(params[:id])
      if @user.nil?
        flash[:notice] = "Cannot find the user"
        redirect_to users_url
      end
    end

end
