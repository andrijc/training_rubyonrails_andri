class ProductsController < ApplicationController
  before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
  before_filter :find_product, :only => [:edit, :update, :show, :destroy]
  def index
    @products = Product.all
  end
  def new
    @product = Product.new
  end
  def create
    @product = Product.new(params[:product])
    if @product.save
      flash[:notice] = "Product was successfully created."
      redirect_to (@product)
    else
      flash[:error] = 'Product was failed to create.'
      render :action => 'new'
    end
  end

  def edit
    @product = Product.find(params[:id])
  end

  def update
    @product = Product.find(params[:id])
    if @product.update_attributes(params[:product])
      flash[:notice] = 'Product was successfully updated.'
      redirect_to(@product)
    else
      flash[:error] = 'Product was failed to update.'
      render :action => "edit"
    end
  end
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def show
      @product = Product.find(params[:id])
  end
  def destroy
    
    @product = Product.find(params[:id])
    if @product.user_id == session[:user_id]
      #flash[:notice] = "Product is deleted."
      @product.destroy ? (flash[:error] = "Product successfully deleted") :
                    (flash[:notice] = "Product failed to delete")
      redirect_to products_url
    else
      @product.destroy ? (flash[:error] = "Product successfully deleted") :
                    (flash[:notice] = "Product failed to delete")
      redirect_to products_url
    end
    
  end
  private
    def find_product
      @product = Product.find_by_id(params[:id])
      if @product.nil?
        flash[:notice] = "Cannot find the product"
        redirect_to products_url
      end
    end
  
end
