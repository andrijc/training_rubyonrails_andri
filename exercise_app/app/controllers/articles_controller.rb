class ArticlesController < ApplicationController
  #before_filter :find_product
  before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
  before_filter :find_article, :only => [:edit, :update, :show, :destroy]
  def index
    @articles = Article.all
  end
  def new
    @article = Article.new
  end
  def create
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "Article was successfully created."
      redirect_to (@article)
    else
      flash[:error] = 'Article was failed to create.'
      render :action => 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = 'Article was successfully updated.'
      redirect_to(@article)
    else
      flash[:error] = 'Article was failed to update.'
      render :action => "edit"
    end
  end
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def show
      @article = Article.find(params[:id])
      @comments = Comment.find_all_by_article_id(params[:id])
      @comment = Comment.new
  end
  def destroy
    
    @article = Article.find(params[:id])
    if @article.user_id == session[:user_id]
      #flash[:notice] = "Article is deleted."
      @article.destroy ? (flash[:error] = "Article successfully deleted") :
                    (flash[:notice] = "Article failed to delete")
      redirect_to articles_url
    else
      @article.destroy ? (flash[:error] = "Article successfully deleted") :
                    (flash[:notice] = "Article failed to delete")
      redirect_to articles_url
    end
    
  end
  private
    def find_article
      @article = Article.find_by_id(params[:id])
      if @article.nil?
        flash[:notice] = "Cannot find the article"
        redirect_to articles_url
      end
    end
end
