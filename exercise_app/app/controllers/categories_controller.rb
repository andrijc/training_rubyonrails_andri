class CategoriesController < ApplicationController
  before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
  before_filter :find_category, :only => [:edit, :update, :show, :destroy]
  def index
    @categories = Category.all
  end
  def new
    @category = Category.new
  end
  def create
    @category = Category.new(params[:category])
    if @category.save
      flash[:notice] = "Category was successfully created."
      redirect_to (@category)
    else
      flash[:error] = 'Category was failed to create.'
      render :action => 'new'
    end
  end

  def edit
    @category = Category.find(params[:id])
  end

  def update
    @category = Category.find(params[:id])
    if @category.update_attributes(params[:category])
      flash[:notice] = 'Category was successfully updated.'
      redirect_to(@category)
    else
      flash[:error] = 'Category was failed to update.'
      render :action => "edit"
    end
  end
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def show
      @category = Category.find(params[:id])
  end
  def destroy
    
    @category = Category.find(params[:id])
    if @category.user_id == session[:user_id]
      #flash[:notice] = "Category is deleted."
      @category.destroy ? (flash[:error] = "Category successfully deleted") :
                    (flash[:notice] = "Category failed to delete")
      redirect_to categories_url
    else
      @category.destroy ? (flash[:error] = "Category successfully deleted") :
                    (flash[:notice] = "Category failed to delete")
      redirect_to categories_url
    end
    
  end
  private
    def find_category
      @category = Category.find_by_id(params[:id])
      if @category.nil?
        flash[:notice] = "Cannot find the category"
        redirect_to categories_url
      end
    end
end
