class Admin::ArticlesController < Admin::ApplicationController
  before_filter :require_admin_login
  def index
    @articles = Article.all
  end
  def new
    @article = Article.new
  end
  def create
    @article = Article.new(params[:article])
    if @article.save
      flash[:notice] = "Creating succeed"
      redirect_to :action => 'index'
    else
      flash[:notice] = "Creating failed"
      render :action => 'new'
    end
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(params[:article])
      flash[:notice] = "Updating succeed"
      redirect_to :action => :index
    else
      flash[:notice] = "Updating failed"
      render :action => :edit
    end
  end
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def show
      @article = Article.find(params[:id])
      @comments = Comment.find_all_by_article_id(params[:id])
      @comment = Comment.new
  end
  def destroy
    @article = Article.find(params[:id])
    if @article.user_id == session[:user_id]
      flash[:notice] = "Article is deleted."
      @article.destroy
      redirect_to admin_articles_path
    else
      flash[:notice] = "Unauthorized to delete the article."
      redirect_to admin_articles_path
    end
    
  end
  
end
