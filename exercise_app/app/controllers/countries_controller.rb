class CountriesController < ApplicationController
  before_filter :require_login, :only => [:index,:new, :create, :edit, :update, :destroy]
  before_filter :find_country, :only => [:edit, :update, :show, :destroy]
  def index
    @countries = Country.all
  end
  def new
    @country = Country.new
  end
  def create
    @country = Country.new(params[:country])
    if @country.save
      flash[:notice] = "Country was successfully created."
      redirect_to (@country)
    else
      flash[:error] = 'Country was failed to create.'
      render :action => 'new'
    end
  end

  def edit
    @country = Country.find(params[:id])
  end

  def update
    @country = Country.find(params[:id])
    if @country.update_attributes(params[:country])
      flash[:notice] = 'Country was successfully updated.'
      redirect_to(@country)
    else
      flash[:error] = 'Country was failed to update.'
      render :action => "edit"
    end
  end
  
  def welcome
    render :text => "Welcome to Ruby on Rails"
  end
  def show
      @country = Country.find(params[:id])
  end
  def destroy
    @country = Country.find(params[:id])
    @country.destroy ? (flash[:error] = "Country successfully deleted") :
                  (flash[:notice] = "Country failed to delete")
    redirect_to countries_url
  end
  private
    def find_country
      @country = Country.find_by_id(params[:id])
      if @country.nil?
        flash[:notice] = "Cannot find the country"
        redirect_to countries_url
      end
    end
end
