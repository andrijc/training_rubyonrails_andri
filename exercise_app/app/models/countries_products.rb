class CountriesProducts < ActiveRecord::Base
   attr_accessible :country_id, :product_id
   belongs_to :countries
   belongs_to :products
end
