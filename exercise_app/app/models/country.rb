class Country < ActiveRecord::Base
   has_many :users
   # scope :indo, where(:name => 'indonesia')
   #has_many :indonesian,
   #         :class_name => "User" ,
   #         :foreign_key => "country_id" ,
   #         :conditions => { :country_id => Country.where("name like'%indonesia%'")[0].id }
            #:conditions => "country_id =5"
   validates :code, :presence => { :message => "You must fill code" }
   validates :name, :presence => { :message => "You must fill name" }
   validate :legal
   def legal
      self.errors[:code] << "can't be filled unless 'id,usa,chn'" unless (code == 'id' || code == 'usa' || code == 'chn')
   end
   attr_accessible :code, :name
end
