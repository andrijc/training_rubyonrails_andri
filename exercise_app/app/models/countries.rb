class Countries < ActiveRecord::Base
   has_many :users
   scope :indo, where(:name => 'indonesia')
   has_many :indonesian,
            :class_name => "Users" ,
            :foreign_key => "countries_id" ,
            :conditions => "countries_id = '1'"
   validates :code, :presence => { :message => "You must fill code" }
   validates :name, :presence => { :message => "You must fill name" }
   validate :legal
   def legal
      self.errors[:code] << "can't be filled unless 'id,usa,chn'" unless (code == 'id' || code == 'usa' || code == 'chn')
   end
   attr_accessible :code, :name
end
