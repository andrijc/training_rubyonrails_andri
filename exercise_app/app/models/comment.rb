class Comment < ActiveRecord::Base
  belongs_to :user
  belongs_to :article
  validates :content, :presence => { :message => "You must fill content" }
  validates :user_id, :presence => { :message => "You must fill user_id" }
  validates :article_id, :presence => { :message => "You must fill article_id" }
  attr_accessible :content, :user_id, :article_id
end
