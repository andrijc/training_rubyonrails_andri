class Comments < ActiveRecord::Base
  belongs_to :users
  belongs_to :articles
  validates :content, :presence => { :message => "You must fill content" }
  validates :users_id, :presence => { :message => "You must fill users_id" }
  validates :articles_id, :presence => { :message => "You must fill articles_id" }
  attr_accessible :content, :users_id, :articles_id
end
