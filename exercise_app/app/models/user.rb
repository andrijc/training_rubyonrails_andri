class User < ActiveRecord::Base
  has_many :products
  has_many :articles
  has_many :comments
  belongs_to :country 
  has_many :my_country,
           :class_name => "Article",
           :foreign_key => "user_id",
           :conditions => "title like '%my country%'"
  
  #scope :indo, lambda { joins(:countries).merge(Country.indo)}
  #scope :indo, where(:country_id=>'5')
  #scope :indo, where(:country_id=>Country.indo[0].id)
  scope :indo, where("countries.name LIKE '%indonesia%'").includes(:country)
  validates :first_name, :presence => { :message => "You must fill first name" },
			:length => {:minimum => 1, :maximum => 25 },
			:uniqueness => true, 
			:format => {:with => /[a-zA-Z\s]+$/}
  validates :last_name, :presence => { :message => "You must fill last name" },
			:length => {:minimum => 1, :maximum => 25 },
			:uniqueness => true, 
			:format => {:with => /[a-zA-Z\s]+$/}
  validates :email, :presence => { :message => "You must fill e-mail" },
			:uniqueness => true, 
			:format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  validates :username, :presence => { :message => "You must fill username" }
  validates :password, :presence => { :message => "You must fill password",:on => :create}, :confirmation => true
  validates :date_of_birth, :presence => { :message => "You must fill date_of_birth" }
  validates :age, :presence => { :message => "You must fill age" }
  validates :address, :presence => { :message => "You must fill address" }
  validates :country_id, :presence => { :message => "You must fill country_id" }
  validates_confirmation_of :password
  def show_full_address
    country = Country.find(self.country_id)
    "#{self.address} #{country.name}"
  end
  def self.age_18_more
     where("age > 18")
  end
  before_save :encrypt_password
  #attr_accessor :password
  attr_accessible :first_name, :last_name, :email, :username, :password, :date_of_birth, :age, :address, :country_id, :password_confirmation, :admin
  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(password, password_salt)
    end
  end
  def self.authenticate(email, password)
    user = find_by_email(email)
    if user && user.password_hash == BCrypt::Engine.hash_secret(password, user.password_salt)
     user
    else
      nil
    end
  end

  def is_admin?
    if self.admin
      return true
    else
      return false
    end
  end
end
