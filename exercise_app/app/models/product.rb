class Product < ActiveRecord::Base
  belongs_to :user
  has_and_belongs_to_many :categories
  #has_many :categories, :through => :categories_products
  scope :stock, lambda { |stock| where('stock = ?' , stock) }
  scope :price_less_than, lambda { |price| where('price < ?' , price) }
  validates :price, :presence => { :message => "You must fill price" }, :numericality => true
  validates :description, :presence => { :message => "You must fill description" }
  validates :name, :presence => { :message => "You must fill name" }
  validates :stock, :presence => { :message => "You must fill stock" }
  validates :user_id, :presence => { :message => "You must fill user_id" }
  attr_accessible :description, :name, :price, :stock, :user_id
end
