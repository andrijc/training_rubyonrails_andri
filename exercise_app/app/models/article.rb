class Article < ActiveRecord::Base
  belongs_to :user
  has_many :comments, :dependent => :destroy
  scope :rating, lambda {|rating| where("rating = ?", rating)}
  attr_accessible :title, :description, :rating, :user_id
  validates :title, :presence => { :message => "You must fill title" },
  		              :uniqueness => true
  validates :description, :presence => { :message => "You must fill description" }
  validates :rating, :presence => { :message => "You must fill rating" }
  validates :user_id, :presence => { :message => "You must fill users_id" }		  
  def self.more_than_100
    where(" length(description) > 100")
  end
end
