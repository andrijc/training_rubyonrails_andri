class Category < ActiveRecord::Base
  #has_many :products, :through => :categories_products
  has_and_belongs_to_many :products
  scope :books, where("name like '%books%'")
  has_and_belongs_to_many :shoes,
           :class_name => "Product" ,
           :foreign_key => "category_id" ,
           :conditions => "name like '%shoes%'"
  validates :name, :presence => { :message => "You must fill name" }
  validates :user_id, :presence => { :message => "You must fill user_id" }
  attr_accessible :name, :user_id
end
