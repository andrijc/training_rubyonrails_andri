class Users < ActiveRecord::Base
  has_many :products
  has_many :articles
  has_many :comments
  belongs_to :countries 
  has_many :my_country,
           :class_name => "Articles",
           :foreign_key => "users_id",
           :conditions => "title like '%my country%'"
  #default_scope where("countries_id = '1'")
  scope :indo, joins(:countries).merge(Countries.indo)
  validates :first_name, :presence => { :message => "You must fill first name" },
			:length => {:minimum => 1, :maximum => 25 },
			:uniqueness => true, 
			:format => {:with => /[a-zA-Z\s]+$/}
  validates :last_name, :presence => { :message => "You must fill last name" },
			:length => {:minimum => 1, :maximum => 25 },
			:uniqueness => true, 
			:format => {:with => /[a-zA-Z\s]+$/}
  validates :email, :presence => { :message => "You must fill e-mail" },
			:uniqueness => true, 
			:format => {:with => /^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i}
  validates :username, :presence => { :message => "You must fill username" }
  validates :password, :presence => { :message => "You must fill password" }
  validates :date_of_birth, :presence => { :message => "You must fill date_of_birth" }
  validates :age, :presence => { :message => "You must fill age" }
  validates :address, :presence => { :message => "You must fill address" }
  validates :countries_id, :presence => { :message => "You must fill countries_id" }
  def show_full_address
    country = Countries.find(self.countries_id)
    "#{self.address} #{country.name}"
  end
  def self.age_18_more
     where("age > 18")
  end
  attr_accessible :first_name, :last_name, :email, :username, :password, :date_of_birth, :age, :address, :countries_id
end
