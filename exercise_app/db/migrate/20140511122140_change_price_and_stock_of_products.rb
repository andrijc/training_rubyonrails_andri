class ChangePriceAndStockOfProducts < ActiveRecord::Migration
  def up
  	change_column :products, :price, :integer
  	change_column :products, :stock, :integer
  end

  def down
  	change_column :products, :price, :string
  	change_column :products, :stock, :string
  end
end
