class AddUserIdToArticles < ActiveRecord::Migration
  def up
    add_column :articles, :users_id, :integer
  end
  def down
    remove_column :articles, :users_id
  end
end
