class ChangeCountriesToCountry < ActiveRecord::Migration
  def up
    rename_column :users, :countries_id, :country_id
  end

  def down
    rename_column :users, :country_id, :countries_id
  end
end
