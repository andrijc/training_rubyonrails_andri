class RenameData < ActiveRecord::Migration
  def up
    rename_column :users, :user_name, :username
    rename_column :comments, :body, :content
  end

  def down
  end
end
