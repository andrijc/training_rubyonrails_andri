class AddCategoryToProducts < ActiveRecord::Migration
  def up
    add_column :products, :categories_id, :integer
    add_column :categories, :users_id, :integer
  end
  def down
    remove_column :products, :categories_id
    remove_column :categories, :users_id
  end
end
