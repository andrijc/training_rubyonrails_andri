class DropTables < ActiveRecord::Migration
  def up
  end

  def down
     drop_table :schema_migrations
     drop_table :users
     drop_table :articles
     drop_table :countries
     drop_table :comments
  end
end
