class RenameBodyInArticles < ActiveRecord::Migration
  def up
     rename_column :articles, :body, :description
  end

  def down
     rename_column :articles, :description, :body
  end
end
