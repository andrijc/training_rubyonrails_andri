class AddCountriesIdToUsers < ActiveRecord::Migration
  def up
    add_column :users, :countries_id, :integer
  end
  def down
    remove_column :users, :countries_id
  end
end
