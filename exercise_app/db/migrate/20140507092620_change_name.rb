class ChangeName < ActiveRecord::Migration
  def up
    rename_column :articles, :users_id, :user_id
    rename_column :categories, :users_id, :user_id
    rename_column :comments, :users_id, :user_id
    rename_column :comments, :articles_id, :article_id
    rename_column :products, :users_id, :user_id
    rename_column :products, :categories_id, :category_id
    
  end

  def down
    rename_column :articles, :user_id, :users_id
    rename_column :categories, :user_id, :users_id
    rename_column :comments, :user_id, :users_id
    rename_column :comments, :article_id, :articles_id
    rename_column :products, :user_id, :users_id
    rename_column :products, :category_id, :categories_id
  end
end
