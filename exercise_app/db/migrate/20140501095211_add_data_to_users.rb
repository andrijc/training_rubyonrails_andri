class AddDataToUsers < ActiveRecord::Migration
  def up
     add_column :users, :date_of_birth, :string
     add_column :users, :age, :integer
     add_column :users, :address, :string
     remove_column :users, :bio_profile
  end
  def down
     remove_column :users, :date_of_birth
     remove_column :users, :age
     remove_column :users, :address
     add_column :users, :bio_profile, :string
  end
end
