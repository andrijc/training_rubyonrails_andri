class AddArticlesIdToComments < ActiveRecord::Migration
  def up
    add_column :comments, :articles_id, :integer
  end
  def down
    remove_column :comments, :articles_id
  end
end
