class AddUserIdToProducts < ActiveRecord::Migration
  def up
    add_column :products, :users_id, :integer 
  end
  def down
    remove_column :products, :users_id
  end
end
